public class VoxelMatrix3D{
	
	public float n11;
	public float n12;
	public float n13;
	public float n14;
	public float n21;
	public float n22;
	public float n23;
	public float n24;
	public float n31;
	public float n32;
	public float n33;
	public float n34;
	public float n41;
	public float n42;
	public float n43;
	public float n44;

	VoxelMatrix3D(){
		this.n11 = 1;
		this.n12 = 0;
		this.n13 = 0;
		this.n14 = 0;
		this.n21 = 0;
		this.n22 = 1;
		this.n23 = 0;
		this.n24 = 0;
		this.n31 = 0;
		this.n32 = 0;
		this.n33 = 1;
		this.n34 = 0;
		this.n41 = 0;
		this.n42 = 0;
		this.n43 = 0;
		this.n44 = 1;
	}

	VoxelMatrix3D(float newN11, float newN12, float newN13, float newN14, float newN21, float newN22, float newN23, float newN24,
				  float newN31, float newN32, float newN33, float newN34, float newN41, float newN42, float newN43, float newN44){
		this.n11 = newN11;
		this.n12 = newN12;
		this.n13 = newN13;
		this.n14 = newN14;
		this.n21 = newN21;
		this.n22 = newN22;
		this.n23 = newN23;
		this.n24 = newN24;
		this.n31 = newN31;
		this.n32 = newN32;
		this.n33 = newN33;
		this.n34 = newN34;
		this.n41 = newN41;
		this.n42 = newN42;
		this.n43 = newN43;
		this.n44 = newN44;
	}

	public VoxelMatrix3D clone(){
		return new VoxelMatrix3D(this.n11, this.n12, this.n13, this.n14, this.n21, this.n22, this.n23, this.n24, this.n31, this.n32, this.n33, this.n34, this.n41, this.n42, this.n43, this.n44);
	}

	public void concatValues(VoxelMatrix3D m){
		
		VoxelMatrix3D values = new VoxelMatrix3D();
		values.n11 = this.n11 * m.n11 + this.n12 * m.n21 + this.n13 * m.n31 + this.n14 * m.n41;
		values.n12 = this.n11 * m.n12 + this.n12 * m.n22 + this.n13 * m.n32 + this.n14 * m.n42;
		values.n13 = this.n11 * m.n13 + this.n12 * m.n23 + this.n13 * m.n33 + this.n14 * m.n43;
		values.n14 = this.n11 * m.n14 + this.n12 * m.n24 + this.n13 * m.n34 + this.n14 * m.n44;
		               
		values.n21 = this.n21 * m.n11 + this.n22 * m.n21 + this.n23 * m.n31 + this.n24 * m.n41;
		values.n22 = this.n21 * m.n12 + this.n22 * m.n22 + this.n23 * m.n32 + this.n24 * m.n42;
		values.n23 = this.n21 * m.n13 + this.n22 * m.n23 + this.n23 * m.n33 + this.n24 * m.n43;
		values.n24 = this.n21 * m.n14 + this.n22 * m.n24 + this.n23 * m.n34 + this.n24 * m.n44;
		               
		values.n31 = this.n31 * m.n11 + this.n32 * m.n21 + this.n33 * m.n31 + this.n34 * m.n41;
		values.n32 = this.n31 * m.n12 + this.n32 * m.n22 + this.n33 * m.n32 + this.n34 * m.n42;
		values.n33 = this.n31 * m.n13 + this.n32 * m.n23 + this.n33 * m.n33 + this.n34 * m.n43;
		values.n34 = this.n31 * m.n14 + this.n32 * m.n24 + this.n33 * m.n34 + this.n34 * m.n44;
		               
		values.n41 = this.n41 * m.n11 + this.n42 * m.n21 + this.n43 * m.n31 + this.n44 * m.n41;
		values.n42 = this.n41 * m.n12 + this.n42 * m.n22 + this.n43 * m.n32 + this.n44 * m.n42;
		values.n43 = this.n41 * m.n13 + this.n42 * m.n23 + this.n43 * m.n33 + this.n44 * m.n43;
		values.n44 = this.n41 * m.n14 + this.n42 * m.n24 + this.n43 * m.n34 + this.n44 * m.n44;

		this.initialize(values);

	}

	public void initialize(VoxelMatrix3D values){
		this.n11 = values.n11;
		this.n12 = values.n12;
		this.n13 = values.n13;
		this.n14 = values.n14;
		this.n21 = values.n21;
		this.n22 = values.n22;
		this.n23 = values.n23;
		this.n24 = values.n24;
		this.n31 = values.n31;
		this.n32 = values.n32;
		this.n33 = values.n33;
		this.n34 = values.n34;
		this.n41 = values.n41;
		this.n42 = values.n42;
		this.n43 = values.n43;
		this.n44 = values.n44;
	}

	public void identity(){
		this.initialize(new VoxelMatrix3D());
	}

	public void rotateX(float angle){
		float sin = sin(angle);
		float cos = cos(angle);
		this.concatValues(new VoxelMatrix3D(
			1, 0, 0, 0,
			0, cos, -sin, 0,
			0, sin, cos, 0,
			0, 0, 0, 1
		));
	}

	public void rotateY(float angle){
		float sin = sin(angle);
		float cos = cos(angle);
		this.concatValues(new VoxelMatrix3D(
			cos, 0, -sin, 0,
			0, 1, 0, 0,
			sin, 0, cos, 0,
			0, 0, 0, 1
		));
	}

	public void rotateZ(float angle){
		float sin = sin(angle);
		float cos = cos(angle);
		this.concatValues(new VoxelMatrix3D(
			cos, sin, 0, 0,
			-sin, cos, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1
		));
	}

	public void scaleMatrix(float sX, float sY, float sZ){
		this.concatValues(new VoxelMatrix3D(
			sX, 0, 0, 0,
			0, sY, 0, 0,
			0, 0, sZ, 0,
			0, 0, 0, 1
		));
	}

	public void translate(float dX, float dY, float dZ){
		this.n41 += dX;
		this.n42 += dY;
		this.n43 += dZ;
	}

	public Voxel3DPoint[] transformPoints(Voxel3DPoint[] points){

		Voxel3DPoint[] newPoints = new Voxel3DPoint[points.length];
		
		for(int i = 0; i < points.length; i++){
			Voxel3DPoint p = points[i];
			Voxel3DPoint newPoint = new Voxel3DPoint(p.x, p.y, p.z);
			newPoint.x = this.n11 * newPoint.x + this.n21*newPoint.y + this.n31 * newPoint.z + this.n41;
			newPoint.y= this.n12 * newPoint.x + this.n22*newPoint.y + this.n32 * newPoint.z + this.n42;
			newPoint.z = this.n13 * newPoint.x + this.n23*newPoint.y + this.n33 * newPoint.z + this.n43;
			newPoints[i] = newPoint;	
		}
		return newPoints;
	}

}