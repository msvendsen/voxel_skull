int width = 600;
int height = 600;
float[][] rawInfo;

float max_rotation;
float focalLength = 1000;
VoxelScene scene;

void setup(){
	size(this.width, this.height);
	
	rawInfo = this.loadXMLFile("voxel_object.xml");

	max_rotation = PI * 0.3;

	scene = new VoxelScene(this.focalLength, this.width, this.height);
	scene.initWithArray(rawInfo);

}

void draw(){
	background(0);
	frameRate(31);
	scene.render();
}

void mouseMoved(){
	float rotationValueY = mouseX / float(this.width) * max_rotation * 2 - max_rotation;
	float rotationValueX = mouseY / float(this.height) * max_rotation * 2 - max_rotation;
	scene.sceneRotationY = rotationValueY;
	scene.sceneRotationX = rotationValueX;
}

float[][] loadXMLFile(String filePath){

	XML xml = loadXML(filePath);
	XML[] xmlChildren = xml.getChildren("position");
	float[][] theInfo = new float[xmlChildren.length][];
	for(int i = 0; i < xmlChildren.length; i++){
		float[] voxelPos = new float[3];
		XML childElement = xmlChildren[i];
		voxelPos[0] = float(childElement.getChildren("x")[0].getContent()) * 12;
		voxelPos[1] = float(childElement.getChildren("y")[0].getContent()) * 12;
		voxelPos[2] = float(childElement.getChildren("z")[0].getContent()) * 12;
		theInfo[i] = voxelPos;
	}
	return theInfo;
}
